all: install-gnome

.PHONY: binary
binary:
	go build -o gnome-xinput-layout-switch -ldflags "-s -w"

.PHONY: install-gnome
install-gnome: binary
	make -C gnome-extension install
	install -s gnome-xinput-layout-switch ${HOME}/bin/
	cp switch-layout.service ${HOME}/.local/share/systemd/user/switch-layout.service
	sed -i 's+$$HOME+${HOME}+g' ${HOME}/.local/share/systemd/user/switch-layout.service
	systemctl --user enable switch-layout
	systemctl --user restart switch-layout

.PHONY: install-x11
install-x11: binary
	sudo install -s gnome-xinput-layout-switch /usr/bin/
	echo "/usr/bin/gnome-xinput-layout-switch" | sudo tee /etc/X11/Xsession.d/99-gnome-xinput-layout-switch
