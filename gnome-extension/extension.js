/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

'use strict';

const { Gio, GLib, Meta } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Main = imports.ui.main;

const SwitchIface =
    '<node>' +
    '   <interface name="ru.softlynx.kb_layout">' +
    '       <method name="Switch">' +
    '       </method>' +
    '   </interface>' +
    '</node>';

class SwitchExtension {
    constructor() {
        this._dbusImpl = Gio.DBusExportedObject.wrapJSObject(SwitchIface, this);
    }

    Switch() {
        imports.ui.status.keyboard.getInputSourceManager()._mruSources[1].activate();
    }

    enable() {
        this._dbusImpl.export(Gio.DBus.session, '/ru/softlynx/kb_layout');
    }

    disable() {
        if (this._dbusImpl) this._dbusImpl.unexport();
    }
}

function init() {
    log(`initializing ${Me.metadata.name}`);
    return new SwitchExtension();
}